package ml.inobmabiki4.havchiq.item;

import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import ml.inobmabiki4.havchiq.HavchiqMod;

public class Items {
    private static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, HavchiqMod.MODID);

    public static void register() {
        REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Item> CHEESE = REGISTRY.register("cheese", () -> new Item(
            new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food(new Food.Builder()
                            .hunger(4)
                            .saturation(5)
                            .build())
    ));
}
